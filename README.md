# Maritime Data Filtering #

This repository contains code used to perform maritime vessel filtering using semantic segmentation and object detection.

### What is this repository for? ###

Performing maritime data filtering. 
Requires:

* TensorFlow 1.15
    * Pip installation: `pip install tensorflow-gpu==1.15`
   
* MXNet 1.6.0
    * Pip installation: `pip install mxnet-cu102==1.6.0`
   
* Requires the gluoncv (https://cv.gluon.ai/) versions of DeepLab, Mask-RCNN, and YOLO.
    * Pip installation: `pip install gluoncv==0.7.0`
    * DeepLab, Mask-RCNN, and YOLO can be accessed via gluoncv (see DeepLabV3 example: https://cv.gluon.ai/build/examples_segmentation/demo_deeplab.html)
   
* See **python_requirements.txt** for the complete list of third-party requirements and their versions required to execute the main script.

* Code Version: 0.3

### Execution

`python3 filter_boat_data.py [IMAGE_LIST_FILE]`

Each line of IMAGE_LIST_FILE should contain the absolute path to one image.

### Who do I talk to? ###

* Kester Duncan, kduncan@objectvideo.com
